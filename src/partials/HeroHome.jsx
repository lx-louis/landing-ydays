import { Link } from 'react-router-dom';
import HeroImage from '../images/septpourcentteam2.png';

function HeroHome() {
  
  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6 relative">

        {/* Hero content */}
        <div className="relative pt-32 pb-10 md:pt-40 md:pb-16">
          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-16">
            <h1 className="h1 mb-4 text-[#2B262D]" data-aos="fade-up">
              Sept Pour Cent - 7%
            </h1>
            <p className="text-xl text-[#2B262D] mb-8" data-aos="fade-up" data-aos-delay="200">
              Bienvenue chez Sept Pour Cent, votre partenaire pour une présence numérique réussie. Allions expertise, passion et solutions sur-mesure pour propulser votre entreprise vers de nouveaux sommets.
            </p>
            <div className="max-w-xs mx-auto sm:max-w-none sm:flex sm:justify-center">
              <div data-aos="fade-up" data-aos-delay="400">
                <Link to="/signup" className="btn text-[#2B262D] bg-[#FFF9E9] hover:bg-[#FEF2D2] w-full mb-4 sm:w-auto sm:mb-0">Contactez-nous</Link>
              </div>

            </div>
          </div>

          {/* Hero image */}
          <div>
            <div className="relative flex justify-center items-center" data-aos="fade-up" data-aos-delay="200">
              <img className="mx-auto" src={HeroImage} width="1024" height="504" alt="Hero" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default HeroHome;
